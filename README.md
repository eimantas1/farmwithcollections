A dairy farmer wishes to manage the breeding of the cows in
his farm. For this reason, he defines the Cow entity (cowId,
nickName). Cows can either give birth to calves by insemination
or end their life span.
Your mission is to create a data structure to support the dairy
farm and the following operations on it (assume you start with
a single cow that is always alive, and that all calves are born
female):
1. GiveBirth (parentCowId, childCowId, childNickName) � adds
a new female calf to the farm.
2. EndLifeSpan (cowId) � removes the cow from the farm.
3. Print farm data � outputs entire farm to the standard output
in a readable manner.