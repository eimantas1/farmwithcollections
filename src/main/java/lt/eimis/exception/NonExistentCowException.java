package lt.eimis.exception;

public class NonExistentCowException extends Exception {
    public NonExistentCowException(String s) {
        super(s);
    }
}
