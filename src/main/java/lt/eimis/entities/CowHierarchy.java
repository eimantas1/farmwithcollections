package lt.eimis.entities;

import lt.eimis.exception.AlreadyExistingCowException;
import lt.eimis.exception.ImmortalCowException;
import lt.eimis.exception.NonExistentCowException;

import java.util.ArrayList;
import java.util.List;

public class CowHierarchy {
    public static final long FIRST_COW_ID = 0;
    public static final String FIRST_COW = "The First Cow";

    private final List<Cow> listOfCows = new ArrayList<>();

    public CowHierarchy() {
        listOfCows.add(new Cow(FIRST_COW_ID, FIRST_COW));
    }


    public void GiveBirth(long parentCowId, long cowId, String name) throws AlreadyExistingCowException, NonExistentCowException {
        if (!cowExists(parentCowId)) {
            throw new NonExistentCowException("Cannot find parent cow with id " + parentCowId + ".");
        }
        if (cowExists(cowId)) {
            throw new AlreadyExistingCowException("Cannot add cow with id " + cowId + " because it already exists.");
        }
        listOfCows.add(new Cow(cowId, name));
    }

    public void EndLifeSpan(long cowId) throws NonExistentCowException, ImmortalCowException {
        if (cowId == FIRST_COW_ID) {
            throw new ImmortalCowException();
        }

        if (!cowExists(cowId)) {
            throw new NonExistentCowException("Cannot delete cow with id " + cowId + " because it does not exits.");
        }
        listOfCows.removeIf(c -> c.getCowId() == cowId);
    }

    public void PrintFarmData() {
        listOfCows.forEach(System.out::println);
    }

    public long getCowCount(){
        return listOfCows.size();
    }

    private boolean cowExists(long cowId) {
        return listOfCows.stream().anyMatch(c -> c.getCowId() == cowId);
    }
}
