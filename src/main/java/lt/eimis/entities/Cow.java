package lt.eimis.entities;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Cow {
    private long cowId;
    private String nickName;
}
