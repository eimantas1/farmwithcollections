package lt.eimis.entities;

import lt.eimis.exception.AlreadyExistingCowException;
import lt.eimis.exception.ImmortalCowException;
import lt.eimis.exception.NonExistentCowException;
import org.junit.Test;

import static lt.eimis.entities.CowHierarchy.FIRST_COW_ID;
import static org.junit.Assert.assertEquals;

public class CowHierarchyTest {

    private CowHierarchy cowHierarchy = new CowHierarchy();


    @Test
    public void sunnyDay() throws NonExistentCowException, AlreadyExistingCowException, ImmortalCowException {
        assertEquals(1, cowHierarchy.getCowCount());
        cowHierarchy.GiveBirth(FIRST_COW_ID, 2, "cow 2");
        cowHierarchy.GiveBirth(2, 3, "cow 3");
        cowHierarchy.GiveBirth(2, 4, "cow 4");
        cowHierarchy.GiveBirth(3, 10, "cow 10");
        assertEquals(5, cowHierarchy.getCowCount());
        cowHierarchy.EndLifeSpan(3);
        assertEquals(4, cowHierarchy.getCowCount());
        cowHierarchy.PrintFarmData();
    }

    @Test(expected = AlreadyExistingCowException.class)
    public void cannotCreateTwoCowsWithTheSameId() throws NonExistentCowException, AlreadyExistingCowException {
        cowHierarchy.GiveBirth(FIRST_COW_ID, 2222, "cow 2222");
        cowHierarchy.GiveBirth(FIRST_COW_ID, 2222, "cow 2222");
    }

    @Test(expected = NonExistentCowException.class)
    public void cannotCreateTwoCowsWithNonExistingParent() throws NonExistentCowException, AlreadyExistingCowException {
        cowHierarchy.GiveBirth(21, 20, "cow 21");
    }

    @Test(expected = NonExistentCowException.class)
    public void cannotEndLifeSpanForNonExistingCow() throws NonExistentCowException, ImmortalCowException {
        cowHierarchy.EndLifeSpan(42);
    }

    @Test(expected = ImmortalCowException.class)
    public void cannotEndLifeSpanForTheVeryFirstCow() throws NonExistentCowException, ImmortalCowException {
        cowHierarchy.EndLifeSpan(FIRST_COW_ID);
    }
}